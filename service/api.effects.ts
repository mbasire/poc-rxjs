import { r, combineRoutes } from "@marblejs/http";
import { mergeMap, delay, zipWith } from "rxjs/operators";
import { randomInt } from "fp-ts/Random";
import { io, date } from "fp-ts";
import { defer, of } from "rxjs";

const randomDelay = defer(io.map((d: number) => of(d))(randomInt(20, 550)));
const timestamp = defer(io.map((d: number) => of(d))(date.now));

const endpoint = (endpoint: string) =>
  r.pipe(
    r.matchPath(endpoint),
    r.matchType("GET"),
    r.useEffect((req$) =>
      req$.pipe(
        zipWith(timestamp, randomDelay),
        mergeMap(([req, t, d]) =>
          of({ body: { requestHeaders: req.headers, endpoint: req.url, timestamp: t }}).pipe(delay(d))
        )
      )
    )
  );

export const api$ = combineRoutes("/", [
  endpoint("re7/*"),
  endpoint("livescore/*"),
  endpoint("meteo/*"),
]);
