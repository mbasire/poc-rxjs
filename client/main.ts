import { zip, of, defer, timer, pipe } from "rxjs";
import * as rx from "rxjs/operators";
import { GET } from "./http";
import { date, io } from "fp-ts";
import { runInContext } from "vm";
import { map } from "fp-ts/lib/Functor";

const re7 = "http://localhost:1337/re7";
const livescore = "http://localhost:1337/livescore";

const timestamp = defer(io.map((d: number) => of(d))(date.now));


// Obserbable => Observable

const recipes = timer(0,300).pipe(
  rx.map(c => `${re7}/${c}`),    
  rx.windowCount(20),
  rx.mergeMap(
        pipe(
          rx.mergeMap(GET),
          rx.zipWith(timestamp),
          rx.timeInterval(),
          rx.tap((r) =>
          console.log(
            `${new Date().toISOString()}: RE7-,\t duration: ${
              r.interval
            },\t latency: ${
              r.value[0].serverTimestamp - r.value[1]
            },\t serverTimeStamp : ${new Date(r.value[0].serverTimestamp).toISOString()}`
          )
        ),
        rx.map( timeInterval => timeInterval.value[0])
      ),
    ),    
);

const sportMatches = GET(livescore).pipe(
  rx.timeInterval(),
  rx.repeat(),
  rx.tap((r) =>
    console.log(
      `${new Date().toISOString()}: ${r.interval}\tLIVESCORE ${JSON.stringify(
        r.value
      )}`
    )
  ),
  rx.map( timeinterval => timeinterval.value ),
  rx.windowTime(1000),
  rx.mergeMap( list => list.pipe(rx.count(_ => true)))
);

recipes.pipe(
   ).subscribe({
  next(x) {
    console.log(`${new Date().toISOString()} #### Re7 : ${x}, SportMatches: \n`);
  },
  error(err) {
    console.error("something wrong occurred: " + err);
  },
  complete() {
    console.log("done");
  },
});
