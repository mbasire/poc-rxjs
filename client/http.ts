import { string } from "fp-ts";
import * as http from "http";
import * as rx from "rxjs";


const keepAliveAgent = new http.Agent({
  keepAlive: true,
  maxSockets: 20,
  maxTotalSockets: 100,
  timeout: 1000, // millis
});

export const GET: (
  endpoint: string
) => rx.Observable<{ endpoint: string; duration: number, serverTimestamp: number }> = (endpoint) =>
  rx.defer(() =>
    new rx.Observable((observer: rx.Observer<string>) => {
      http.get(endpoint, { agent: keepAliveAgent }, (response) => {
        response.on("data", (chunk) => observer.next(chunk));
        response.on("close", () => observer.complete());
      });
    }).pipe(
      rx.reduce(string.Monoid.concat),
      rx.map( raw => JSON.parse(raw)),
      rx.timeInterval(),
      rx.map((result) => {
        return {
          endpoint: result.value["endpoint"],
          duration: result.interval,
          serverTimestamp: result.value["timestamp"]
        };
      })
    )
  );
